﻿using System;
using System.Threading;

public class SimpleThreadExample
{
    static void Main(string [] args) {
        int num ;
        Console.WriteLine("Enter Factorial Number");
        string numStr = Console.ReadLine();
        if (!int.TryParse(numStr, out num))
        {
            Console.WriteLine("Please enter valid input for Number ! ");
            return;
        }



        int result1 = 1;
        Thread t1 = new Thread(() => Calculate(1, num/2, ref result1, 1));
        t1.Start();

        int result2 = 1;
        Thread t2 = new Thread(() => Calculate((num/2)+1, num, ref result2, 2));
        t2.Start();

        t1.Join();
        t2.Join();

        int result = result1 * result2;
        Console.WriteLine("Result  is " + result);
    }

    private static void Calculate(int num1, int num2, ref int result, int no) {
        
        for (int i = num1; i <= num2; i++)
            result *= i;
        Console.WriteLine("thread # " + no + " Result " + result);
    }

}